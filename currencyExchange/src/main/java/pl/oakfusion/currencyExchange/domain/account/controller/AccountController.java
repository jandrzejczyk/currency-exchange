package pl.oakfusion.currencyExchange.domain.account.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.oakfusion.currencyExchange.domain.account.dto.DefaultAccountData;
import pl.oakfusion.currencyExchange.domain.account.dto.UsdAccountData;
import pl.oakfusion.currencyExchange.domain.account.service.AccountService;

@RestController
@RequestMapping(value = "/api/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping(value = "/create-account")
    public ResponseEntity createUsdAccount(@RequestBody DefaultAccountData defaultAccountData) {
        var accountState = accountService.createUser(defaultAccountData);

        if (accountState.isCreated()) {
            return ResponseEntity.ok(accountState);
        } else {
            return ResponseEntity.badRequest().body(accountState);

        }
    }

    @PostMapping(value = "/create-usd-account")
    public ResponseEntity createDefaultAccount(@RequestBody UsdAccountData usdAccountData) {
        var accountState = accountService.createUsdAccount(usdAccountData);

        if (accountState.isCreated()) {
            return ResponseEntity.ok(accountState);
        } else {
            return ResponseEntity.badRequest().body(accountState);

        }
    }

    @GetMapping(value = "/get-account-data/{pesel}")
    public ResponseEntity getWallet(@PathVariable String pesel) {

        var details = accountService.getWalletDetails(pesel);

        if (details != null) {
            return ResponseEntity.ok(details);
        } else {
            return ResponseEntity.status(404).body("Account not found");
        }

    }


}
