package pl.oakfusion.currencyExchange.domain.account.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Component
public class AgeChecker {


    boolean checkAge(String pesel) {
        var peselArray = pesel.toCharArray();

        char monthFirstChar = peselArray[2];
        char monthSecondChar = peselArray[3];
        var sb = new StringBuilder();
        String monthsString = sb.append(monthFirstChar).append(monthSecondChar).toString();

        int monthsNumber = Integer.parseInt(monthsString);

        boolean isAdult;
        if (monthsNumber > 13) {
            isAdult = checkAgePersonAfterMillennium(peselArray, monthsNumber);
        } else {
            isAdult = checkAgePersonFromLastCentury(peselArray);
        }

        return isAdult;
    }


    private boolean checkAgePersonAfterMillennium(char[] peselArray, int monthsNumber) {

        buildMonthsNumberWithoutMileniumChecksum(peselArray, monthsNumber);

        StringBuilder sb = getDateFormat(peselArray);

        String date = "20" + sb;


        return checkIsPersonAdult(date);
    }

    private void buildMonthsNumberWithoutMileniumChecksum(char[] peselArray, int monthsNumber) {
        monthsNumber = monthsNumber - 20;

        String months;
        if (monthsNumber < 13) {
            months = "0" + monthsNumber;
        } else {
            months = String.valueOf(monthsNumber);

        }
        var monthsArray = months.toCharArray();
        peselArray[2] = monthsArray[0];
        peselArray[3] = monthsArray[1];
    }

    private StringBuilder getDateFormat(char[] peselArray) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            if (i != 0 && i % 2 == 0) {
                sb.append("-");
            }
            sb.append(peselArray[i]);
        }
        return sb;
    }


    private boolean checkAgePersonFromLastCentury(char[] peselArray) {
        StringBuilder sb = getDateFormat(peselArray);

        String date = "19" + sb;

        return checkIsPersonAdult(date);
    }

    private boolean checkIsPersonAdult(String date) {

        LocalDate birthDay = LocalDate.parse(date);

        LocalDate adultThreshold = LocalDate.now().minus(18, ChronoUnit.YEARS);

        return !birthDay.isAfter(adultThreshold);
    }

}
