package pl.oakfusion.currencyExchange.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreatingAccountResponseData {
    private String name;
    private String surname;
    private boolean created;
    private String message;


}