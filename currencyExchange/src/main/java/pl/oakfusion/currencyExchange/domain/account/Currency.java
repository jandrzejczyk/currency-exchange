package pl.oakfusion.currencyExchange.domain.account;

public enum Currency {

    PLN,
    USD
}
