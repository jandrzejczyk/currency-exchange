package pl.oakfusion.currencyExchange.domain.account;

public class AccountState {

    public final static String ACCOUNT_EXIST = "Account already exist";
    public final static String USER_DOES_NOT_EXIST = "User does not exist";
    public final static String NOT_ENOUGH_MONEY_ON_ACCOUNT = "User does not have enough money";
    public final static String SUB_ACCOUNT_DOES_NOT_EXIST = "Sub account does not exist";
    public final static String ACCOUNT_CREATED = "Account created";
    public final static String CANT_CREATE_ACCOUNT = "Can't create account";
    public final static String USER_IS_NOT_AN_ADULT = "User is not an adult";
    public final static String PESEL_IS_INVALID = "Invalid pesel";
    public final static String SOMETHING_WENT_WRONG = "Something went wrong";
}
