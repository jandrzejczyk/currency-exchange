package pl.oakfusion.currencyExchange.domain.account.jpa;


import lombok.*;
import pl.oakfusion.currencyExchange.domain.EntityBase;
import pl.oakfusion.currencyExchange.domain.account.Currency;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Builder
@Table(name = "currency")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CurrencyEntity  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long currency_id;

//    @Id
    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private AccountEntity account;

    @Column(name = "type")
    private String type;

    @Column(name = "amount")
    private Double amount;


}
