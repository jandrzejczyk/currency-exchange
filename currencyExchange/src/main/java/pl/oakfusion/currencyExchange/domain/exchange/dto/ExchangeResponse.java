package pl.oakfusion.currencyExchange.domain.exchange.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class ExchangeResponse {
    private String message;
    private Double pln;
    private Double usd;

    private boolean isExchanged;

    public ExchangeResponse(Double pln, Double usd, boolean isExchanged) {
        this.pln = pln;
        this.usd = usd;
        this.isExchanged = isExchanged;
    }
}