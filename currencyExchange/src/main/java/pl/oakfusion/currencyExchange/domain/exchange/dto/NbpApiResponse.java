package pl.oakfusion.currencyExchange.domain.exchange.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NbpApiResponse {

    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;
}
