package pl.oakfusion.currencyExchange.domain.exchange.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;
import pl.oakfusion.currencyExchange.domain.account.Currency;
import pl.oakfusion.currencyExchange.domain.account.jpa.AccountEntity;
import pl.oakfusion.currencyExchange.domain.account.jpa.AccountRepository;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeRequestDataWithCurrency;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeResponse;
import pl.oakfusion.currencyExchange.domain.exchange.service.strategy.ExchangePlnStrategy;
import pl.oakfusion.currencyExchange.domain.exchange.service.strategy.ExchangeStrategy;
import pl.oakfusion.currencyExchange.domain.exchange.service.strategy.ExchangeUsdStrategy;
import pl.oakfusion.currencyExchange.domain.exchange.webclient.NbpWebClient;

import java.util.HashMap;
import java.util.Map;

import static pl.oakfusion.currencyExchange.domain.account.AccountState.*;
import static pl.oakfusion.currencyExchange.domain.account.Currency.USD;

@Component
public class CurrencyExchangeService {

    //todo TEST THIS SERVICE

    private final NbpWebClient nbpWebClient;
    private final Map<Currency, ExchangeStrategy> strategiesMap = new HashMap<>();
    private final AccountRepository accountRepository;

    public CurrencyExchangeService(NbpWebClient nbpWebClient, AccountRepository accountRepository) {
        this.nbpWebClient = nbpWebClient;
        this.accountRepository = accountRepository;
        initStrategies();
    }

    private void initStrategies() {
        strategiesMap.put(Currency.PLN, new ExchangePlnStrategy());
        strategiesMap.put(USD, new ExchangeUsdStrategy());
    }

    public ExchangeResponse exchange(ExchangeRequestDataWithCurrency requestData) {

        var account = accountRepository.findByPesel(requestData.getPesel());
        if (account.isPresent()) {
            var isExchangePossible = checkIfExchangePossible(requestData, account.get());
            if (isExchangePossible.isPossible) {
                Double rate = nbpWebClient.fetchCurrencyData();
                var exchangeResponse = strategiesMap.get(requestData.getCurrency()).exchange(requestData, rate, account.get());
                accountRepository.saveAndFlush(account.get());
                return exchangeResponse;
            } else {
                return new ExchangeResponse(isExchangePossible.getMessage(), 0.0, 0.0, false);
            }
        } else {
            return new ExchangeResponse(USER_DOES_NOT_EXIST, 0.0, 0.0, false);
        }

    }

    private ExchangePossibility checkIfExchangePossible(ExchangeRequestDataWithCurrency requestData, AccountEntity accountEntity) {

        var currencyEntity = accountEntity.getCurrencyEntities()
                .stream()
                .filter(el -> el.getType().equals(requestData.getCurrency().toString()))
                .findFirst();

        var usdAccount = accountEntity.getCurrencyEntities()
                .stream()
                .filter(el -> el.getType().equals(USD.toString()))
                .findFirst();

        if (usdAccount.isEmpty()) {
            return new ExchangePossibility(false, SUB_ACCOUNT_DOES_NOT_EXIST);
        } else {
            return currencyEntity
                    .map(entity -> entity.getAmount() > requestData.getAmount() ?
                            new ExchangePossibility(true) :
                            new ExchangePossibility(false, NOT_ENOUGH_MONEY_ON_ACCOUNT))
                    .orElseGet(() -> new ExchangePossibility(false, SUB_ACCOUNT_DOES_NOT_EXIST));

        }
    }

    @AllArgsConstructor
    @Getter
    static class ExchangePossibility {
        private boolean isPossible;
        private String message;

        public ExchangePossibility(boolean isPossible) {
            this.isPossible = isPossible;
        }
    }

}
