package pl.oakfusion.currencyExchange.domain.exchange.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.currencyExchange.domain.account.Currency;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class ExchangeRequestDataWithCurrency {
    private String pesel;
    private Double amount;
    private Currency currency;


}