package pl.oakfusion.currencyExchange.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DefaultAccountData {
    private String name;
    private String surname;
    private String pesel;
    private Double amount;
}