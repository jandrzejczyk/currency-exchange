package pl.oakfusion.currencyExchange.domain.exchange.service.strategy;

import pl.oakfusion.currencyExchange.domain.account.jpa.AccountEntity;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeRequestDataWithCurrency;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static pl.oakfusion.currencyExchange.domain.account.Currency.PLN;
import static pl.oakfusion.currencyExchange.domain.account.Currency.USD;

public class ExchangePlnStrategy implements ExchangeStrategy {
    @Override
    public ExchangeResponse exchange(ExchangeRequestDataWithCurrency requestData, Double rate, AccountEntity accountEntity) {

        BigDecimal bigDecimalPln = BigDecimal.valueOf(requestData.getAmount()).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal bigDecimalRate = BigDecimal.valueOf(rate).setScale(2, RoundingMode.HALF_EVEN);

        var additionalValueToUsd = bigDecimalPln.divide(bigDecimalRate, 2, RoundingMode.HALF_EVEN);


        Double newPlnValue = changePln(accountEntity, bigDecimalPln);
        Double newUsdValue = changeUsd(accountEntity, additionalValueToUsd);

        return new ExchangeResponse(newPlnValue, newUsdValue, true);
    }


    private Double changePln(AccountEntity accountEntity, BigDecimal valueToSubstract) {
        var plnCurrency = accountEntity
                .getCurrencyEntities()
                .stream()
                .filter(el -> el.getType().equals(PLN.toString()))
                .findFirst();

        if (plnCurrency.isPresent()) {
            BigDecimal accountPlnBigDecimal = BigDecimal.valueOf(plnCurrency.get().getAmount()).setScale(2, RoundingMode.HALF_EVEN);
            plnCurrency.get().setAmount(accountPlnBigDecimal.subtract(valueToSubstract).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
            return plnCurrency.get().getAmount();
        } else {
            return null;
        }
    }

    private Double changeUsd(AccountEntity accountEntity, BigDecimal additionalValueToUsd) {
        var usdCurrency = accountEntity
                .getCurrencyEntities()
                .stream()
                .filter(el -> el.getType().equals(USD.toString()))
                .findFirst();

        if (usdCurrency.isPresent()) {
            BigDecimal accountUsdBigDecimal = BigDecimal.valueOf(usdCurrency.get().getAmount()).setScale(2, RoundingMode.HALF_EVEN);
            ;
            usdCurrency.get().setAmount(accountUsdBigDecimal.add(additionalValueToUsd).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
            return usdCurrency.get().getAmount();
        } else {
            return null;
        }
    }
}
