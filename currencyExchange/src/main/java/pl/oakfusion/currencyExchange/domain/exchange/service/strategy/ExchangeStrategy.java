package pl.oakfusion.currencyExchange.domain.exchange.service.strategy;

import pl.oakfusion.currencyExchange.domain.account.jpa.AccountEntity;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeRequestDataWithCurrency;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeResponse;

public interface ExchangeStrategy {

   ExchangeResponse exchange(ExchangeRequestDataWithCurrency requestData, Double rate, AccountEntity accountEntity);
}
