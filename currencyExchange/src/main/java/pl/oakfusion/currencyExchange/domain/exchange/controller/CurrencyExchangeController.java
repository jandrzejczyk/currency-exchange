package pl.oakfusion.currencyExchange.domain.exchange.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.oakfusion.currencyExchange.domain.account.Currency;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeRequestData;
import pl.oakfusion.currencyExchange.domain.exchange.dto.ExchangeRequestDataWithCurrency;
import pl.oakfusion.currencyExchange.domain.exchange.service.CurrencyExchangeService;

import static pl.oakfusion.currencyExchange.domain.account.AccountState.SOMETHING_WENT_WRONG;

@RestController
@RequestMapping(value = "/api/exchange")
@RequiredArgsConstructor
public class CurrencyExchangeController {

    private final CurrencyExchangeService currencyExchangeService;


    @PostMapping(value = "/usd-to-pln")
    public ResponseEntity exchangeUsd(@RequestBody ExchangeRequestData requestData) {

        var exchangeRequestDataWithCurrency = new ExchangeRequestDataWithCurrency(requestData.getPesel(), requestData.getAmount(), Currency.USD);

        return exchange(exchangeRequestDataWithCurrency);

    }

    @PostMapping(value = "/pln-to-usd")
    public ResponseEntity exchangePln(@RequestBody ExchangeRequestData requestData) {

        var exchangeRequestDataWithCurrency = new ExchangeRequestDataWithCurrency(requestData.getPesel(), requestData.getAmount(), Currency.PLN);

        return exchange(exchangeRequestDataWithCurrency);
    }

    private ResponseEntity<?> exchange(ExchangeRequestDataWithCurrency exchangeRequestDataWithCurrency) {
        var response = currencyExchangeService.exchange(exchangeRequestDataWithCurrency);

        if (response != null && response.isExchanged()) {
            return ResponseEntity.ok(response);
        } else if (response != null) {
            return ResponseEntity.badRequest().body(response.getMessage());
        } else {
            return ResponseEntity.badRequest().body(SOMETHING_WENT_WRONG);
        }
    }


}
