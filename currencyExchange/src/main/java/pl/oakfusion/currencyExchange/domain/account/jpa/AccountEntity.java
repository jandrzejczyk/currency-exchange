package pl.oakfusion.currencyExchange.domain.account.jpa;


import lombok.*;
import pl.oakfusion.currencyExchange.domain.EntityBase;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Table(name = "account")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccountEntity  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "pesel", unique = true)
    private String pesel;

    @OneToMany(mappedBy = "account")
    private List<CurrencyEntity> currencyEntities;

}
