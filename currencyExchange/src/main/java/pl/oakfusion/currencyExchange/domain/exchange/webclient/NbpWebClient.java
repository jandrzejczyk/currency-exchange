package pl.oakfusion.currencyExchange.domain.exchange.webclient;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import pl.oakfusion.currencyExchange.domain.exchange.dto.NbpApiResponse;
import pl.oakfusion.currencyExchange.exception.BadRequestException;
import pl.oakfusion.currencyExchange.exception.ClientException;
import pl.oakfusion.currencyExchange.exception.ServerException;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Component
@RequiredArgsConstructor
public class NbpWebClient {

    private final WebClient webClient;
    private final static String USD = "usd";
    private final static long TIMEOUT = 30000;

    public Double fetchCurrencyData() {

        NbpApiResponse nbpApiResponse = null;

        nbpApiResponse = webClient
                .get()
                .uri(USD)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, response -> Mono.error(new ClientException(response.statusCode().toString())))
                .onStatus(HttpStatus::is5xxServerError, response -> Mono.error(new ServerException(response.statusCode().toString())))
                .bodyToMono(NbpApiResponse.class)
                .blockOptional(Duration.ofMillis(TIMEOUT))
                .orElseThrow(BadRequestException::new);

        if (!nbpApiResponse.getRates().isEmpty() && nbpApiResponse.getRates().get(0) != null &&
                nbpApiResponse.getRates().get(0).getMid() != null) {
            return nbpApiResponse.getRates().get(0).getMid();

        } else {
            return null;
        }
    }
}
