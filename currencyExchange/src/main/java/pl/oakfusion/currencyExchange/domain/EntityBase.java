package pl.oakfusion.currencyExchange.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class EntityBase {

//    @Column(name = "timestamp")
//    @UpdateTimestamp
//    private LocalDateTime timestamp;
}
