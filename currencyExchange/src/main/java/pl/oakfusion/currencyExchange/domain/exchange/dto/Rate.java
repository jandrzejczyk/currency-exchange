package pl.oakfusion.currencyExchange.domain.exchange.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Rate {
    private String no;
    private String effectiveDate;
    private Double mid;
}
