package pl.oakfusion.currencyExchange.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountDetailsResponseData {
    private String name;
    private String surname;
    private String pesel;
    private Map<String, Double> wallet;


}