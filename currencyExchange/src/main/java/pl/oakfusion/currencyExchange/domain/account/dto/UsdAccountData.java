package pl.oakfusion.currencyExchange.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UsdAccountData {
    private String name;
    private String surname;
    private String pesel;
}