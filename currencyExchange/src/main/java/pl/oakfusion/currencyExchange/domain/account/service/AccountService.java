package pl.oakfusion.currencyExchange.domain.account.service;

import lombok.RequiredArgsConstructor;
import org.hibernate.validator.internal.constraintvalidators.hv.pl.PESELValidator;
import org.springframework.stereotype.Service;
import pl.oakfusion.currencyExchange.domain.account.Currency;
import pl.oakfusion.currencyExchange.domain.account.dto.AccountDetailsResponseData;
import pl.oakfusion.currencyExchange.domain.account.dto.CreatingAccountResponseData;
import pl.oakfusion.currencyExchange.domain.account.dto.DefaultAccountData;
import pl.oakfusion.currencyExchange.domain.account.dto.UsdAccountData;
import pl.oakfusion.currencyExchange.domain.account.jpa.AccountEntity;
import pl.oakfusion.currencyExchange.domain.account.jpa.AccountRepository;
import pl.oakfusion.currencyExchange.domain.account.jpa.CurrencyEntity;
import pl.oakfusion.currencyExchange.domain.account.jpa.CurrencyRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static pl.oakfusion.currencyExchange.domain.account.AccountState.*;
import static pl.oakfusion.currencyExchange.domain.account.Currency.PLN;
import static pl.oakfusion.currencyExchange.domain.account.Currency.USD;

@RequiredArgsConstructor
@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;
    private final CurrencyRepository currencyRepository;

    private final AgeChecker ageChecker;

    public CreatingAccountResponseData createUser(DefaultAccountData userData) {

        var isPeselValid = checkIsPeselValid(userData.getPesel());

        boolean isAdult;

        if (isPeselValid) {
            isAdult = ageChecker.checkAge(userData.getPesel());
        } else {
            return new CreatingAccountResponseData(userData.getName(), userData.getSurname(), false, PESEL_IS_INVALID);
        }

        if (isAdult) {
            return createDefaultAccount(new DefaultAccountData(userData.getName(), userData.getSurname(), userData.getPesel(), userData.getAmount()));
        } else {
            return new CreatingAccountResponseData(userData.getName(), userData.getSurname(), false, USER_IS_NOT_AN_ADULT);
        }
    }

    private boolean checkIsPeselValid(String pesel) {
        PESELValidator validator = new PESELValidator();
        validator.initialize(null);
        return validator.isValid(pesel, null);
    }

    public CreatingAccountResponseData createDefaultAccount(DefaultAccountData defaultAccountData) {

        var account = accountRepository.findByPesel(defaultAccountData.getPesel());

        if (account.isPresent()) {
            return new CreatingAccountResponseData(defaultAccountData.getName(), defaultAccountData.getSurname(), false, ACCOUNT_EXIST);
        } else {
            var entity = accountRepository.saveAndFlush(AccountEntity.builder()
                    .name(defaultAccountData.getName())
                    .pesel(defaultAccountData.getPesel())
                    .surname(defaultAccountData.getSurname())
                    .build());


            createCurrency(defaultAccountData.getAmount(), entity, PLN.toString());

            return new CreatingAccountResponseData(defaultAccountData.getName(), defaultAccountData.getSurname(), true, ACCOUNT_CREATED);
        }
    }

    public CreatingAccountResponseData createUsdAccount(UsdAccountData usdAccountData) {

        var account = accountRepository.findByPesel(usdAccountData.getPesel());

        if (account.isPresent()) {

            var usdAccount = account
                    .get()
                    .getCurrencyEntities()
                    .stream()
                    .anyMatch(el -> el.getType().equals(Currency.USD.toString()));


            if (usdAccount) {
                return new CreatingAccountResponseData(usdAccountData.getName(), usdAccountData.getSurname(), false, ACCOUNT_EXIST);
            } else {

                createCurrency(0.0, account.get(), USD.toString());
                return new CreatingAccountResponseData(usdAccountData.getName(), usdAccountData.getSurname(), true, ACCOUNT_CREATED);
            }
        } else {
            return new CreatingAccountResponseData(usdAccountData.getName(), usdAccountData.getSurname(), false, USER_DOES_NOT_EXIST);

        }
    }

    private void createCurrency(Double amount, AccountEntity entity, String currency) {
        currencyRepository.saveAndFlush(CurrencyEntity.builder()
                .account(entity)
                .amount(BigDecimal.valueOf(amount).setScale(2, RoundingMode.HALF_EVEN).doubleValue())
                .type(currency)
                .build());
    }

    public AccountDetailsResponseData getWalletDetails(String pesel) {

        var account = accountRepository.findByPesel(pesel);

        return account

                .map(accountEntity -> {

                    Map<String, Double> walletData = new HashMap<>();

                    accountEntity.getCurrencyEntities().forEach(el -> {
                        walletData.put(el.getType(), el.getAmount());
                    });

                    return new AccountDetailsResponseData(accountEntity.getName(), accountEntity.getSurname(),
                            accountEntity.getPesel(), walletData);
                })
                .orElse(null);

    }
}



