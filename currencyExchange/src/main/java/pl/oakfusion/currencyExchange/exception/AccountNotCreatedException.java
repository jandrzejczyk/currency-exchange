package pl.oakfusion.currencyExchange.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AccountNotCreatedException extends RuntimeException {
}
