package pl.oakfusion.currencyExchange.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserNotExistException extends RuntimeException {
}
