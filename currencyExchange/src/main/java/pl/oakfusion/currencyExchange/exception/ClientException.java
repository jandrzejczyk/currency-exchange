package pl.oakfusion.currencyExchange.exception;

public class ClientException extends RuntimeException {

    public ClientException(String message) {
        super(message);
    }
}
