# EXCHANGE SERVICE

This service is responsible for exchange money between two sub-accounts of the user.

## API

To communicate with this service you need to use api.

### Create user

```/api/account/create-account```
This is an endpoint to create user. User need to be an adult and have proper pesel.
To do it properly you also need body, which looks like the one below:

```
{
	"name":"John",
	"surname": "Doe",
	"pesel": "49092551540",
	"amount": "102.234"
}
```

### Create Usd sub-account
```/api/account/create-usd-account```
This is an endpoint to create usd sub-account. You can't create sub-account without having user with same pesel. RequestBody for this request looks like:
```
{
	"name":"John",
	"surname": "Doe",
	"pesel": "49092551540"
}
```

### Get information about your account
```/api/account/get-account-data/{pesel}```

Using this request you can check information about user and his account ballance.

### Exchange
You can exchange money from usd to pln and from pln to usd. To do it you need these endpoints:
```/api/exchange/usd-to-pln```
```/api/exchange/pln-to-usd```

with body:
```
{
	"pesel": "49092551540",
	"amount": "20.25"	
}
```